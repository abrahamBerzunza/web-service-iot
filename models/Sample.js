const mongoose = require('mongoose')
const Schema = mongoose.Schema

const sampleSchema = new Schema({
  date: String,
  signal: Array,
  features: Object
})

const Sample = mongoose.model('Sample', sampleSchema)
module.exports = Sample
