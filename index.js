require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const moment = require('moment')
const { PythonShell } = require('python-shell')

const port = process.env.PORT || 3000
const app = express()
// MODELS
const Sample = require('./models/Sample')

mongoose.connect(process.env.DB_URI, { useNewUrlParser: true })
const db = mongoose.connection

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors({
  origin: '*',
  methods: ['GET', 'POST']
}))

let signalsParsed = []

app.get('/sample', function (req, res) {
  const data = req.query
  console.log('DATOS:', data)

  if (!data.signal) {
    return res.status(400).send('data not founded')
  }

  const signals = data.signal.split(',')
  signals.pop()

  signals.forEach(function (signal) {
    signalsParsed.push(signal)
  })

  if (data.last === 'true') {
    console.log('SEÑAL', signalsParsed)
    let options = {
      args: [
        JSON.stringify({ signal: signalsParsed }),
        '250',
        '11250'
      ]
    }

    PythonShell.run('./ECGFeatureExtractor/ECGFeatureExtractor.py', options, function (err, results) {
      if (err) {
        console.log('ERROR: ', err)
        return res.status(500).json({ message: 'INTERNAL_SERVER_ERROR' })
      }

      console.log('RESULT: ', results)

      let index = results.length - 1
      const features = JSON.parse(results[index])

      console.log('FEATURES DATA: ', features)

      const sample = new Sample({
        date: moment().format('L'),
        signal: signalsParsed,
        features
      })

      sample.save(function (err) {
        if (err) {
          console.log('ERROR IN SAVE DATA:', err)
          return res.status(500).send('Internal Server Error')
        }

        console.log('DATA SAVED IN DATABASE')
        // Clear array signals parsed
        signalsParsed = []
      })
    })
  }

  console.log('SENT OK')
  return res.status(200).send('ok')
})

app.get('/samples', function (req, res) {
  const date = req.query.date

  if (!date) {
    return res.status(404).json({ message: 'params: data not founded' })
  }

  Sample.find({ date }, function (err, docs) {
    if (err) {
      console.log('ERROR: ', err)
    }

    return res.status(200).json({
      data: docs
    })
  })
})

app.get('/', function (req, res) {
  return res.send('connected')
})

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connection mongodb successful')
  app.listen(port, function () {
    console.log('server running ...')
  })
})
